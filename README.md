Tutorials and exercises regarding C++ and Asio 1.10.8 library.

The tutorial pages:
https://think-async.com/Asio/asio-1.11.0/doc/asio/tutorial.html

Mostly copy pasted solutions from Asio tutorial pages, but does contain
some playing around. I made some modifications to test how things work out,
because the goal was to learn how things behave and work out.
