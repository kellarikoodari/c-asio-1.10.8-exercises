#include <iostream>
#include <asio.hpp>
#include <boost/array.hpp>

using asio::ip::tcp;

int main(int argc, char *argv[])
{
    try
    {
        if (argc != 2)
        {
            std::cerr << "Usage: client <host>" << std::endl;
            std::cerr << "For example use https://tf.nist.gov/tf-cgi/servers.cgi for server addresses" << std::endl;
            return 1;
        }

        asio::io_service io_service;
        tcp::resolver resolver(io_service);

        tcp::resolver::query query(argv[1], "daytime");
        tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);

        tcp::socket socket(io_service);
        asio::connect(socket, endpoint_iterator);

        for (;;)
        {
            boost::array<char, 128> buf;
            asio::error_code error;

            size_t len = socket.read_some(asio::buffer(buf), error);

            if (error == asio::error::eof)
                break; // connection closed by the peer
            else if (error)
            {
                throw asio::system_error(error); // Unhandled error
            }

            std::cout.write(buf.data(), len);
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }
}
