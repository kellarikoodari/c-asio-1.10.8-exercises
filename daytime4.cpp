#include <iostream>
#include <asio.hpp>
#include <boost/bind.hpp>
#include <boost/array.hpp>

using asio::ip::udp;

int main(int argc, char *argv[])
{
    try
    {
        if (argc != 2)
        {
            std::cout << "Usage: client <host>" << std::endl;
            return 1;
        }

        asio::io_service io_service;
        udp::resolver resolver(io_service);
        udp::resolver::query query(udp::v4(), argv[1], "daytime");

        udp::endpoint receiver_endpoint = *resolver.resolve(query);

        udp::socket socket(io_service);
        socket.open(udp::v4());

        boost::array<char, 1> send_buf = {{0}};
        socket.send_to(asio::buffer(send_buf), receiver_endpoint);

        boost::array<char, 128> recv_buf;
        udp::endpoint sender_endpoint;

        size_t len = socket.receive_from(
            asio::buffer(recv_buf), sender_endpoint);
        std::cout << "Message length is: " << len << std::endl;
        std::cout.write(recv_buf.data(), len);
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }

    return 0;
}
