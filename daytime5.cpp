#include <iostream>
#include <asio.hpp>
#include <boost/array.hpp>
#include "utils.h"

using asio::ip::udp;

int main()
{
    try
    {
        asio::io_service io_service;
        udp::socket socket(io_service, udp::endpoint(udp::v4(), 13));

        for (;;)
        {
            boost::array<char, 1> recv_buf;
            udp::endpoint remote_endpoint;

            asio::error_code error;

            socket.receive_from(asio::buffer(recv_buf), remote_endpoint, 0, error);

            if (error && error != asio::error::message_size)
            {
                throw asio::system_error(error);
            }

            std::string message = make_daytime_string();

            asio::error_code ignored_error;
            socket.send_to(asio::buffer(message), remote_endpoint, 0, ignored_error);
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }

    return 0;
}
