#include <iostream>
#include <asio.hpp>
#include <boost/bind.hpp>
#include <boost/array.hpp>

#include "utils.h"
#include "tcp_service.h"
#include "udp_service.h"

int main()
{
    try
    {
        asio::io_service io_service;
        tcp_server server1(io_service);
        udp_server server2(io_service);
        io_service.run();
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }

    return 0;
}
