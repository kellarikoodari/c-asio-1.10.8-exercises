#include <iostream>
#include <asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

void print(const asio::error_code & /*e*/)
{
    std::cout << "Hello world 2 after 5 seconds" << std::endl;
}

int main()
{
    asio::io_service io;
    asio::deadline_timer t(io, boost::posix_time::seconds(5));

    t.async_wait(&print);
    t.async_wait(boost::bind(&print, asio::placeholders::error)); // needed the error parameter value in this

    io.run(); // will not continue from this unless all tasks are done
    std::cout << "Async task finished" << std::endl;
    return 0;
}
