#include <iostream>
#include <asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/bind.hpp>

void print(const asio::error_code & /*e*/, asio::deadline_timer *t, int *iterations)
{
    std::cout << *iterations << std::endl;

    if (*iterations > 0)
    {
        // --(*iterations);

        t->expires_at(t->expires_at() + boost::posix_time::seconds(1));
        t->async_wait(boost::bind(print, asio::placeholders::error, t, &--(*iterations)));

        // --(*iterations);
    }
}

int main()
{
    asio::io_service io;
    int iterations = 5;
    asio::deadline_timer t(io, boost::posix_time::seconds(1));

    t.async_wait(boost::bind(print, asio::placeholders::error, &t, &iterations));

    io.run();
    std::cout << "Iterations left: " << iterations << std::endl;

    return 0;
}
