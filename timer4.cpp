#include <iostream>
#include <asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

// void root_print(const asio::error_code & /*e*/)
// {
//     std::cout << "root print" << std::endl;
// }

class printer
{
public:
    printer(asio::io_service &io) : timer_(io, boost::posix_time::seconds(1)), count_(0)
    {
        timer_.async_wait(boost::bind(&printer::print, this));
    }

    ~printer()
    {
        std::cout << "The final count is " << count_ << std::endl;
    }

    void print()
    {
        if (count_ < 5)
        {
            std::cout << count_ << std::endl;
            count_++;

            timer_.expires_at(timer_.expires_at() + boost::posix_time::seconds(1));
            timer_.async_wait(boost::bind(&printer::print, this));

            // timer_.async_wait(&root_print);
            // timer_.async_wait(this->print); // Cannot pass a pointer to a member function without
            // the context of the functions this.

            // void (printer::*print_ptr)() = &printer::print;
            // timer_.async_wait(&print_ptr); // still a pointer to a member function without a context

            // auto encapsulated_function_context = [member_context = this](const asio::error_code & /*e*/)
            // { member_context->print(); };
            // timer_.async_wait(encapsulated_function_context);

            // auto my_lambda = [](const asio::error_code & /*e*/)
            // { std::cout << "My own lambda" << std::endl; };
            // timer_.async_wait(my_lambda);
        }
    }

private:
    asio::deadline_timer timer_;
    int count_;
};

int main()
{
    asio::io_service io;
    printer p(io);
    io.run();
    return 0;
}
