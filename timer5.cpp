#include <iostream>
#include <asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

class printer
{
// DELAY_SEC could not be set as a constant member variable for printer, as it would not be there
// in compilation time, making the class constructor interpret DELAY_SEC as 0
#define DELAY_SEC 1

// If defined, then each print function will wait for the other to finish before starting their own coroutine
// If not defined, then it is expected print1 and print2 functions will print over and top of each others output
// when DELAY_SEC is 0
#define WAIT_FOR_OTHER_THREAD

// Defines how many times each print call repeats its print
#define REPEATS 10

// Defines the length of the output for prints. The longer it is the more likely there are any race conditions visible
#define MAXIMUM_COUNT 200

public:
    printer(asio::io_service &io)
        : strand_(io),
          timer1_(io, boost::posix_time::seconds(DELAY_SEC)),
          timer2_(io, boost::posix_time::seconds(DELAY_SEC)),
          count_(0)
    {
        std::cout << "Begin with count " << count_ << std::endl;

#ifdef WAIT_FOR_OTHER_THREAD
        timer1_.async_wait(strand_.wrap(boost::bind(&printer::print1, this)));
        timer2_.async_wait(strand_.wrap(boost::bind(&printer::print2, this)));
#else
        timer1_.async_wait(boost::bind(&printer::print1, this));
        timer2_.async_wait(boost::bind(&printer::print2, this));
#endif
    }

    ~printer()
    {
        std::cout << "The final count is: " << count_ << std::endl;
    }

    void print1()
    {
        if (count_ < MAXIMUM_COUNT)
        {
            for (int i = 0; i < REPEATS; i++)
            {
                std::cout << "Timer 1: " << count_ << std::endl;
                ++count_;
            }

            timer1_.expires_at(timer1_.expires_at() + boost::posix_time::seconds(DELAY_SEC));

#ifdef WAIT_FOR_OTHER_THREAD
            timer1_.async_wait(strand_.wrap(boost::bind(&printer::print1, this)));
#else
            timer1_.async_wait(boost::bind(&printer::print1, this));
#endif
        }
    }

    void print2()
    {
        if (count_ < MAXIMUM_COUNT)
        {
            for (int i = 0; i < REPEATS; i++)
            {
                std::cout << "Timer 2: " << count_ << std::endl;
                ++count_;
            }

            timer2_.expires_at(timer2_.expires_at() + boost::posix_time::seconds(DELAY_SEC));

#ifdef WAIT_FOR_OTHER_THREAD
            timer2_.async_wait(strand_.wrap(boost::bind(&printer::print2, this)));
#else
            timer2_.async_wait(boost::bind(&printer::print2, this));
#endif
        }
    }

private:
    asio::io_service::strand strand_;
    asio::deadline_timer timer1_;
    asio::deadline_timer timer2_;
    int count_;
};

int main()
{
    asio::io_service io;
    printer p(io);

    // Remember to install libpthread
    asio::thread t(boost::bind(&asio::io_service::run, &io));

    io.run();
    t.join();

    return 0;
}
