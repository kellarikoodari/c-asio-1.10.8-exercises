#include <iostream>
#include <asio.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include "utils.h"

using asio::ip::udp;

class udp_server
{
private:
    udp::socket socket_;
    udp::endpoint remote_endpoint_;
    boost::array<char, 1> recv_buffer_;

    void start_receive()
    {
        std::cout << "udp server starts receiving" << std::endl;
        socket_.async_receive_from(asio::buffer(recv_buffer_), remote_endpoint_,
                                   boost::bind(&udp_server::handle_receive,
                                               this,
                                               asio::placeholders::error,
                                               asio::placeholders::bytes_transferred));
    }

    void handle_receive(const asio::error_code &error, std::size_t /*bytes received*/)
    {
        if (!error || error == asio::error::message_size)
        {
            std::cout << "udp server handle received" << std::endl;
            boost::shared_ptr<std::string> message(
                new std::string(make_daytime_string()));
            socket_.async_send_to(asio::buffer(*message),
                                  remote_endpoint_,
                                  boost::bind(&udp_server::handle_send,
                                              this,
                                              message,
                                              asio::placeholders::error,
                                              asio::placeholders::bytes_transferred));
        }

        start_receive();
    }

    void handle_send(boost::shared_ptr<std::string> /*message*/,
                     const asio::error_code & /*error*/,
                     std::size_t /*bytes_transferred*/)
    {
    }

public:
    udp_server(asio::io_service &io_service) : socket_(io_service, udp::endpoint(udp::v4(), 13))
    {
        std::cout << "udp server started" << std::endl;
        start_receive();
    }
};
