#ifndef __UTILS__H__
#define __UTILS__H__

#include <iostream>

std::string make_daytime_string()
{
    using namespace std; // For time_t, time and ctime;
    time_t now = time(0);
    return ctime(&now);
}

#endif //!__UTILS__H__
